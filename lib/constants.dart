import 'package:flutter/material.dart';

class Constants {
  static const int side = 20;
  static const int nbFood = 8;
  static const int speed = 300;

  static const int minSnake = 4;

  static const Color color = Colors.orange;
  static final Color foodColor = Colors.orange[300];
  static const Color loseColor = Colors.red;
}